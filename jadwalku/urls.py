from django.urls import path
from . import views

app_name = 'jadwalku'

urlpatterns = [
    path('', views.index, name='index'),
    path('<int:id>', views.detail, name='detail'),
    path('hapus/<int:id>', views.delete_matkul, name='hapus'),
    path('tambah-jadwal', views.tambah, name='tambah'),
]
