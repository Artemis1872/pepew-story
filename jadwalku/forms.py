from django import forms
from .models import MataKuliah
from crispy_forms.helper import FormHelper

class MataKuliahForm(forms.ModelForm):
    helper = FormHelper()
    
    class Meta:
        model = MataKuliah
        fields = "__all__"
    
    # nama_matkul = forms.CharField(
    #     label="Nama Mata Kuliah", 
    #     error_messages=["Mohon isi nama mata kuliah"], 
    #     max_length=, 
    #     required=False
    # )
    # deskripsi   = forms.Textarea()
    # kelas       = forms.CharField()
    # kredit      = forms.IntegerField(max_value=6, min_value=1)
    # pengajar    = forms.CharField()
    # semester    = forms.ChoiceField(, choices=[CHOICES], required=False)
    