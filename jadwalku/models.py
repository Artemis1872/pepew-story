from django.utils.translation import gettext_lazy as _
from django.urls import reverse
from django.db import models

# Create your models here.
class Semester(models.Model):
    
    name = "Semester"

    class Term(models.TextChoices):
        GENAP = 'Genap', _('Genap')
        GANJIL = 'Ganjil', _('Ganjil')
    
    term = models.CharField(max_length=6, choices=Term.choices, default=Term.GANJIL)
    tahun_ajaran = models.CharField(max_length=9) 

    class Meta:
        verbose_name = _("Semester")
        verbose_name_plural = _("Semesters")

    def __str__(self):
        return '%s %s' % (self.term, self.tahun_ajaran)

    def get_absolute_url(self):
        return reverse("Semester_detail", kwargs={"pk": self.pk})


class MataKuliah(models.Model):
    
    name = "MataKuliah"

    SKS = [
        (1, '1'),
        (2, '2'),
        (3, '3'),
        (4, '4'),
        (5, '5'),
        (6, '6'),
    ]

    nama_matkul = models.CharField(max_length=50)
    deskripsi   = models.TextField()
    kelas       = models.CharField(max_length=50)
    kredit      = models.IntegerField(choices=SKS, default=4)
    pengajar    = models.CharField(max_length=100)
    semester    = models.ForeignKey("Semester", verbose_name="semester", on_delete=models.CASCADE, default="Ganjil 2020/2021")

    waktu_dibuat= models.DateField(auto_now_add=True)

    class Meta:
        verbose_name = "MataKuliah"
        verbose_name_plural = "MataKuliahs"

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("MataKuliah_detail", kwargs={"pk": self.pk})
