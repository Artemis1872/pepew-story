from django.shortcuts import render, redirect
from .forms import MataKuliahForm
from .models import MataKuliah

# Create your views here.
def index(request):
    mata_kuliah = MataKuliah.objects.all()
    context = {'mata_kuliah' : mata_kuliah}
    return render(request, 'jadwalku.html', context=context)

def tambah(request):
    if request.method == "POST":
        form = MataKuliahForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('jadwalku:index')
    else:
        context = {
            'form': MataKuliahForm(),
        }
        return render(request, 'tambah-jadwal.html', context=context)
    
def detail(request, id):
    context = {
        'matkul' : MataKuliah.objects.get(id=id)
    }
    return render(request, 'jadwalku-detail.html', context=context)

def delete_matkul(request, id):
    MataKuliah.objects.get(id=id).delete()
    return redirect('jadwalku:index')

def redirek(request):
    return redirect('jadwalku:index')
