from django.urls import path
from . import views

app_name = 'bridge'

urlpatterns = [
    path('', views.bridge, name='bridge'),
]