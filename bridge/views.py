from django.shortcuts import render

# Create your views here.

def bridge(request):
    return render(request, 'bridge.html')
